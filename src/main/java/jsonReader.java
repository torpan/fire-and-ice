import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class jsonReader {
    public JSONObject json;
    // This function makes a GET request and stores the results into JSON
    public boolean makeGetRequest(String requestURL) {
        try{
            HttpURLConnection con = (HttpURLConnection) (new URL(requestURL)).openConnection();
            con.setRequestMethod("GET");
            if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();
                json = new JSONObject(content.toString());
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
