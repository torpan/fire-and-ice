import java.util.Scanner;

public class ProgramFireAndIce {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of the character you want to look up");
        String userInput = sc.next();

        //Using the input to creta the correct connection.
        jsonReader characterjson = new jsonReader();
        characterjson.makeGetRequest("https://www.anapioficeandfire.com/api/characters/"+userInput);
        System.out.println(characterjson.json.getString("name"));

        //This is the basic info I choose to print out.
        System.out.println("Name: "+characterjson.json.getString("name"));
        System.out.println("Gender: "+characterjson.json.getString("gender"));
        System.out.println("Culture: "+characterjson.json.getString("culture"));
        System.out.println("Born: "+characterjson.json.getString("born")+"\n");

        //Checking for allegiances, using that url to make new connections.
        //Printing out the name of the House..
        for(Object house : characterjson.json.getJSONArray("allegiances")) {
            jsonReader housejson = new jsonReader();
            housejson.makeGetRequest((String)house);
            System.out.println(housejson.json.getString("name"));

            //.. and using the members url to make new connections and printing out the name of the sworn members.
            for(Object character : housejson.json.getJSONArray("swornMembers")){
                jsonReader member = new jsonReader();
                member.makeGetRequest((String)character);
                System.out.println(member.json.getString("name"));
            }
            System.out.println("");
        }

        //While there is a next book to look in, check if the publisher is Bantam Books.
        //If it is, get to that url and print out the povCharacters.
        jsonReader publisherjson = new jsonReader();
        int i = 1;
        System.out.println("Books published by Bantam Books, and their POV-Characters");
        while(publisherjson.makeGetRequest("https://www.anapioficeandfire.com/api/books/"+i)){
            if(publisherjson.json.getString("publisher").contains("Bantam Books")){
                System.out.println("Book name: "+publisherjson.json.getString("name"));
                for(Object povCharacters : publisherjson.json.getJSONArray("povCharacters")) {
                    jsonReader povcharacterjson = new jsonReader();
                    povcharacterjson.makeGetRequest((String) povCharacters);
                    System.out.println(povcharacterjson.json.getString("name"));
                }
            }
            i ++;
        }
        System.out.println("");
    }
}

